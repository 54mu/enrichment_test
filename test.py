import sys 
import pandas as pd
import numpy as np
from scipy.stats import hypergeom, rankdata
import json

def fdr(pvalues):
    ranked_ps = rankdata(pvalues)
    fdr = pvalues * len(pvalues) / ranked_ps
    fdr[fdr > 1 ] = 1
    return fdr

def bonferroni(pvalues, alpha):
    # conta le p < alfa
    np = len(pvalues)
    # calcola bonferroni
    bonfy = pvalues*np
    bonfy[bonfy > 1] = 1
    return bonfy

def holm(pvalues, alpha):
    ranked_ps = rankdata(pvalues)
    holm = pvalues * (len(pvalues) + 1 - ranked_ps)
    holm[holm > 1 ] = 1
    return holm


####### controllo l'input 
if len(sys.argv) < 4:
    print("invalid arguments: \nusage is: {} annotation_table subset_list genome_size".format(sys.argv[0]))
    quit()

####### caricamento dei parametri 

proteome_annotation_file = sys.argv[1]
subset_file = sys.argv[2]
proteome_size = int(sys.argv[3])

####### lettura della tabella con pfam e id 

annotated_proteome = pd.read_csv(proteome_annotation_file, sep = "\t", names= ["seq_id","pfam_id"]).drop_duplicates()
#annotated_proteome.pfam_id = annotated_proteome.pfam_id.apply(lambda x: x.replace(" ", ""))
print(annotated_proteome.head())

####### lettura del file con il subset

observed_list = set() # usiamo un set per ottimizzare il tempo di esecuzione
print(list(observed_list)[0:10])
with open(subset_file, "r") as subset: # con questo blocco il file viene aperto in lettura, letto e poi chiuso
    for line in subset:
        observed_list.add(line.rstrip()) # per ogni linea del file leggo l'id corrispondente, tolgo il ritorno a capo e lo aggiungo al set degli osservati

####### creazione delle tabelle di conta dei pfam
print("doot")
print(list(observed_list)[0:10])
pfam_dict = {} # uso un dizionario per raccogliere le conte relative a ciascun id PFAM

for pfamid in (annotated_proteome.pfam_id): # popolazione del dizionario, per ogni elemento
    try:                                    # prova ad incrementare la conta,
        pfam_dict[pfamid] += 1 
    except:                                 # se non esiste allora conta la prima occorrenza
        pfam_dict[pfamid] = 1

####### conversione del dizionario in dataframe
####### creazione del dataframe delle osservazioni

observed_dataframe = annotated_proteome[annotated_proteome.seq_id.isin(observed_list)] # usando il dataframe del proteoma si annotano le sequenze del subset

####### creazione del dataframe delle osservazioni

observed_dict = {}
for pfamid in (observed_dataframe.pfam_id):
    #print(pfamid)
    try: 
        observed_dict[pfamid] += 1 
    except:
        observed_dict[pfamid] = 1

universe_dataset = pd.DataFrame.from_dict(pfam_dict, orient = "index", columns=["total_counts"])
#print(observed_dict)
obs_dataset = pd.DataFrame.from_dict(observed_dict, orient = "index", columns=["observed_counts"]) 
print(obs_dataset.head())

####### unione dei due dataframe

dataframe = universe_dataset.merge(obs_dataset, how =  "left", left_index=True, right_index=True )

dataframe = dataframe.fillna(0) # sostituzione dei NaN con 0


####### test ipergeometrico 

N = dataframe.total_counts.sum() + (proteome_size - len(set(annotated_proteome.seq_id)))
n = dataframe.observed_counts.sum()

dataframe["pvalue"] = hypergeom.sf(dataframe.observed_counts, N, dataframe.total_counts, n)

#print(dataframe[(dataframe.pvalue < 0.05) & (dataframe.observed_counts > 0)])
print(dataframe)
op = dataframe[(dataframe.observed_counts > 0)].sort_values(by = "pvalue")

op["FDR"] = fdr(op["pvalue"])
op["Bonferroni"] = bonferroni(op["pvalue"], 0.05)
op["Holm-Bonferroni"] = holm(op["pvalue"], 0.05)
op["Obs-Exp"] = op["observed_counts"] - (op["total_counts"] / proteome_size * len(observed_list))
#op = op[op.pvalue < 0.05] 

######## aggiunta annotazioni leggibili

######## caricamento database e annotazione


def get_node(GO_id):
    try:
        GO_id = "http://purl.obolibrary.org/obo/" + GO_id.replace(":", "_").replace(" ", "")
    except:
        return({'id': str(GO_id), 'class' : "NA", 'description' : "NA"})
    accepted_cats = {"biological_process", "cellular_component", "molecular_function"}
    for layer in range(len(go_db_js["graphs"])):
        for node in go_db_js["graphs"][layer]["nodes"]:
            if node['id'] == GO_id:
                cat = ""
                parent = ""
                for i in node["meta"]['basicPropertyValues']:
                    if i['val'] in accepted_cats:
                        cat = i['val']
                    else:
                        continue
                    if "GO:" in i['val']:
                        parent = i['val']
                    else:
                        continue
                try:
                    return({'id': GO_id, 'class' : cat, 'description' : node['lbl']})
                except Exception as e:
                    print(e)
                    print(GO_id)
                    return({'id': GO_id, 'class' : cat, 'description' : get_node(parent)})
            else:
                pass
    return({'id': GO_id, 'class' : "NA", 'description' : "NA"})

d = {}

if "GO:" in list(op.index)[1]:
    op["GOID"] = op.index
    try:
        op["GO_description"] = op["GOID"].apply(lambda x : get_node(x)['description'])
    except Exception as e:
        print(e)
    op["class"] = op["GOID"].apply(lambda x : get_node(x)['class'])
    op.drop(columns=["GOID"])
else:
    print("this is not a list of GO ids, will not try to recover extra info")
    

op.to_csv(sys.argv[2] + "_enrichment_test.tsv", sep = "\t" )


