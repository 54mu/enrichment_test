# Enrichment_test

Simple script for gene set enrichment analysis. To run it use:

```bash
python test.py universe subset <unique_elements_in_universe>
```
where universe is a tab-separated file of unique id-to-feature mappings, subset is the list of ids to test and unique_elements_in_universe is the total number of features in the universe (including the non-annotated ones)
